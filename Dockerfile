ARG NGINX_TAG=alpine

FROM nginx:${NGINX_TAG}

LABEL traefik.enable="true"
LABEL traefik.http.routers.rxap-catch-all.rule="PathPrefix(`/api`)"
LABEL traefik.http.routers.rxap-catch-all.priority="1"

COPY default.conf.template /etc/nginx/templates/default.conf.template
