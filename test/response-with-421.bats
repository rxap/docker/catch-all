#!/usr/bin/env bats

load ./load.bash

@test "should response with 405 for CONNECT request" {
  run http --print=h CONNECT "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 405 Not Allowed"
}

@test "should response with 421 for DELETE request" {
  run http --print=h DELETE "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 421 Misdirected Request"
}

@test "should response with 421 for GET request" {
  run http --print=h GET "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 421 Misdirected Request"
}

@test "should response with 421 for HEAD request" {
  run http --print=h HEAD "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 421 Misdirected Request"
}

@test "should response with 421 for OPTIONS request" {
  run http --print=h OPTIONS "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 421 Misdirected Request"
}

@test "should response with 421 for PATCH request" {
  run http --print=h PATCH "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 421 Misdirected Request"
}

@test "should response with 421 for POST request" {
  run http --print=h POST "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 421 Misdirected Request"
}

@test "should response with 421 for PUT request" {
  run http --print=h PUT "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 421 Misdirected Request"
}

@test "should response with 405 for TRACE request" {
  run http --print=h TRACE "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 405 Not Allowed"
}
